terragrunt = {
  terraform {
    source = "git::git@github.com:terraform-aws-modules/terraform-aws-security-group.git"
  }

  include = {
    path = "${find_in_parent_folders()}"
  }

  
}

# Name of security group
# type: string
name = "clean-kit"

# ID of the VPC where to create security group
# type: string
vpc_id = ""


